from flask import Flask
import sheet
import json
from flask import request

app = Flask(__name__)


@app.route('/')
def all():
    print("a")
    print(sheet.semua())
    a = {"semua":sheet.semua()}
    return json.dumps(a)

@app.route('/tambah')
def tambah():
    nama = request.args.get("nama")
    nominal = request.args.get("nominal")
    jenis = request.args.get("jenis")
    sheet.nambahin(nama=nama, nominal=nominal, jenis=jenis)
    return "OK"

@app.route('/edit')
def edit():
    ida = request.args.get("id")
    nama = request.args.get("nama")
    nominal = request.args.get("nominal")
    jenis = request.args.get("jenis")
    sheet.edit(id=ida, nama=nama, nominal=nominal, jenis=jenis)
    return "OK"

@app.route('/hapus')
def hapus():
    id = request.args.get("id")
    sheet.hapus(id)
    return("OK")

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')