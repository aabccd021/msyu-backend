import gspread
from oauth2client.service_account import ServiceAccountCredentials
import datetime

scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('msyu-ed8e7bd1b293.json', scope)
client = gspread.authorize(creds)
sheet = client.open("tagihan").sheet1

dateTimeStringStandard = '%Y-%m-%d %H:%M:%S'

def nambahin(nama, nominal, jenis):
    last = int(sheet.acell("E1").value)
    last+=1
    waktu = datetime.datetime.now().strftime(dateTimeStringStandard)
    sheet.update_acell("E1", str(last))
    sheet.append_row([nama, nominal, jenis, waktu, last])

def edit(id, nama, nominal, jenis):
    hapus(id)
    nambahin(nama, nominal, jenis)
    
def hapus(id):
    rowvals = sheet.col_values(5)
    rowvals.pop(0)
    print(rowvals)
    rownum = rowvals.index(str(id))+2
    print(rownum)
    sheet.delete_row(rownum)


def semua():
    all = sheet.get_all_values()
    all.pop(0)
    return all

if __name__ == "__main__":
    print("a")
